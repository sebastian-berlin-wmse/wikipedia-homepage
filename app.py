import json
import logging
import re
import yaml

from flask import Flask
from flask import abort
from flask import g
from flask import redirect
from flask import render_template
from flask import request
from flask import url_for
import flask_babel
from flask_babel import Babel
from flask_babel import _
import requests
from requests import Timeout
from werkzeug.exceptions import HTTPException
from bs4 import BeautifulSoup
from bs4 import Comment

app = Flask(__name__)
babel = Babel(app)
language = "sv"

with open("config.yaml") as config_file:
    app.config.update(yaml.safe_load(config_file))


@app.route("/")
def start(banner=None):
    search_language = request.args.get("language")
    if (
            not search_language or
            app.config.get("search_languages", {}).get(search_language) is None
    ):
        search_language = language

    footer = app.config.get("footer", [])
    attributions = []
    if "wikipedia_logo_attribution" in app.config:
        attributions.append(app.config.get("wikipedia_logo_attribution"))
    for block in footer:
        if "attribution" in block:
            attributions.append(block["attribution"])

    search_languages = app.config.get("search_languages", {})
    search_language_parameters = search_languages.get(search_language)
    search_language_parameters["code"] = search_language
    if "placeholder" not in search_language_parameters:
        default_language = app.config.get("search_languages", {}).get(language)
        search_language_parameters["placeholder"] = default_language.get("placeholder")

    if banner is None:
        with open("templates/banner.html") as banner_file:
            banner = banner_file.read()

    return render_template(
        "index.html",
        lang=language,
        search_languages=search_languages,
        search_language=search_language_parameters,
        footer=footer,
        attributions=attributions,
        banner=banner
    )

@app.route("/suggest")
def suggest():
    language = request.args.get("lang")
    search = request.args.get("search")
    url = f"https://{language}.wikipedia.org/w/api.php"
    parameters = {
        "action": "query",
        "list": "allpages",
        "apnamespace": 0,
        "aplimit": 10,
        "apprefix": search,
        "format": "json"
    }
    try:
        response = requests.get(url, params=parameters, timeout=1.0).json()
    except Timeout as e:
        logging.error("Timeout for request to Wikipedia API.", exc_info=e)
        abort(504)
    except Exception as e:
        logging.error("Error for request to Wikipedia API.", exc_info=e)
        abort(500)
    suggestions = [p["title"] for p in response["query"]["allpages"]]
    return suggestions

@app.errorhandler(HTTPException)
def handle_exception(e):
    response = e.get_response()
    response.data = json.dumps({
        "code": e.code,
        "name": e.name,
        "description": e.description,
    })
    response.content_type = "application/json"
    return response

@app.route("/go")
def go():
    language = request.args.get("l")
    query = request.args.get("q")
    url = f"http://{language}.wikipedia.org/wiki/{query}"
    return redirect(url)

@app.route("/preview-banner")
def preview():
    url = request.args.get("url")
    allowed_pattern = app.config.get("banner", {}).get("allowed_url_pattern")
    if allowed_pattern and not re.match(allowed_pattern, url):
        message = _("Invalid banner URL. Should match <code>%(allowed_pattern)s</code>.", allowed_pattern=allowed_pattern)
        banner_html = render_template("banner-error.html", message=message)
        return start(banner=banner_html)

    selector = request.args.get("selector")
    banner_html = get_banner_html(url, selector)
    return start(banner=banner_html)

def get_banner_html(url=None, selector=None):
    """Generate HTML for the banner

    Uses url and selector from parameters if given and values from
    config if not.

    """
    banner = app.config.get("banner", {})
    if url is None:
        url = banner.get("url")
        if not url:
            message = "Missing config variable <code>banner: url</code>."
            return render_template("banner-error.html", message=message)

    if selector is None:
        selector = banner.get("selector")

    response = requests.get(url).text
    if selector:
        soup = BeautifulSoup(response, "html.parser")
        html = soup.select(selector)[0]
    else:
        html = response

    return html

@app.route("/set-banner")
def set_banner():
    banner = app.config.get("banner", {})
    url = banner.get("url")
    selector = banner.get("selector")
    html = get_banner_html(url, selector)
    with open("templates/banner.html", "w") as f:
        f.write(str(html))

    return start()

@babel.localeselector
def get_locale():
    return language
